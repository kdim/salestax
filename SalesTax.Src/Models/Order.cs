﻿using System.Collections.Generic;

namespace SalesTax.Src
{
    public class Order
    {
        public List<IOrderItem<TaxableItem>> OrderItems { get; set; } = new List<IOrderItem<TaxableItem>>();


        public void AddItem(int number, TaxableItem product)
        {
            OrderItems.Add(new OrderItem(number, product));
        }
    }
}
