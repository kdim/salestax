﻿
namespace SalesTax.Src
{
    public class OrderItem: IOrderItem<TaxableItem>
    {
        public int NumberOnCheckout { get; set; }
        public TaxableItem Product { get; set; }

        public OrderItem(int number, TaxableItem product)
        {
            NumberOnCheckout = number;
            Product = product;
        }
    }
}
