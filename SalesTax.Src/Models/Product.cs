﻿using System;
using System.Collections.Generic;

namespace SalesTax.Src
{
    public class Product: TaxableItem
    {
        public Product(string name, decimal price)
        {
            Name = name;
            Price = price;
            ApplicableTaxes = new Dictionary<string, decimal>();
        }

    }
}
