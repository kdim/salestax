﻿

namespace SalesTax.Src
{
    public class ImportTaxApplicator : ITaxApplicator<TaxableItem>
    {

        public void ApplyTax(TaxableItem item, bool isImported)
        {
            if (isImported) 
            { 
                item.ApplicableTaxes.Add("import_sales_tax", 5); 
            };
        }
    }
}
