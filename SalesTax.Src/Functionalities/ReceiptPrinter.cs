﻿using System;
using System.Collections.Generic;

namespace SalesTax.Src
{
    public class ReceiptConsolePrinter: ReceiptOutputHandler
    {
        public ReceiptConsolePrinter(List<IOrderItem<TaxableItem>> orderItems, decimal totalSalesTax, decimal totalAfterTax) 
            : base(orderItems, totalSalesTax, totalAfterTax){}
        public override void Output()
        {
            Console.WriteLine("\nYOUR RECEIPT: \n");
            foreach (IOrderItem<TaxableItem> item in OrderItems)
            {
                Console.WriteLine(item.NumberOnCheckout + ": " + item.Product.Name + ": " + String.Format("{0:0.00}", item.Product.CostAfterTax()));
            }

            Console.WriteLine("Sales Taxes: " + String.Format("{0:0.00}", TotalSalesTax) + " Total: " + String.Format("{0:0.00}", TotalAfterTax));

        }
    }
}
