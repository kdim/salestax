﻿

namespace SalesTax.Src
{
    public class RegularTaxApplicator : ITaxApplicator<Product>
    {
        public void ApplyTax(Product item, bool isExempt)
        {
            if (!isExempt) 
            {
                item.ApplicableTaxes.Add("regular_sales_tax", 10);
            };

        }
    }
}
