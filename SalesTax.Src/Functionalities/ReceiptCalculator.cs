﻿using System;
using System.Collections.Generic;

namespace SalesTax.Src
{
    public class ReceiptCalculator
    {
        
        public decimal TotalSalesTax;
        public decimal TotalAfterTax;
        private List<IOrderItem<TaxableItem>> _order;

        private void CalculateTotalSalesTax()
        {
            TotalSalesTax = 0;
            foreach (IOrderItem<TaxableItem> item in _order)
            {
                TotalSalesTax += Math.Round(item.NumberOnCheckout*item.Product.SalesTax(),2); 
            }
        }

        private void CalculateTotalAfterTax()
        {
            TotalAfterTax = 0;
            foreach (IOrderItem<TaxableItem> item in _order)
            {
                TotalAfterTax += Math.Round(item.NumberOnCheckout * item.Product.CostAfterTax(), 2); 
            }
        }

        public void ProcessOrder(List<IOrderItem<TaxableItem>> order)
        {
            _order = order;
            CalculateTotalSalesTax();
            CalculateTotalAfterTax();
        }

    }
}
