﻿
using System.Collections.Generic;

namespace SalesTax.Src
{ 
    public interface IProduct
    {
        public decimal Price { get; set; }
        public Dictionary<string, decimal> ApplicableTaxes { get; set; }
        public decimal SalesTax();
        public decimal CostAfterTax();
    }
}
