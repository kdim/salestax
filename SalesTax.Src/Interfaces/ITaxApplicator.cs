﻿

namespace SalesTax.Src
{
    public interface ITaxApplicator<T>
    {
        void ApplyTax(T item, bool criteria);
    }
}
