﻿

namespace SalesTax.Src
{
    public interface IOrderItem<T>
    {
        int NumberOnCheckout { get; set; }
        T Product { get; set; }
    }
}
