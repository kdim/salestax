﻿using System;
using System.Collections.Generic;

namespace SalesTax.Src
{
    class Program
    {


        static void Main()
        {

            bool ordersComplete = false;
            var receiptCalculator = new ReceiptCalculator();

            var order = new Order();
            Console.WriteLine("NEW ORDER STARTED");
            var regularTax = new RegularTaxApplicator();
            var importTax = new ImportTaxApplicator();

            while (!ordersComplete)
            {
                
                Console.WriteLine("Enter product name:");
                string name = Console.ReadLine();
                int number = ValidatedIntPrompt("How many of these?");
                decimal price = ValidatedDecimalPrompt("Enter price in USD:",2); 
                var isImported = ValidatedYesNoPrompt("Is the item imported?");
                var isExempt = ValidatedYesNoPrompt("Is the item a book, food, or a medical product?");
                var item = new Product(name, price);
                regularTax.ApplyTax(item, isExempt);
                importTax.ApplyTax(item, isImported);

                order.AddItem(number, item);
                if (ValidatedYesNoPrompt("Is the order complete?"))
                {
                    receiptCalculator.ProcessOrder(order.OrderItems);
                    var printer = new ReceiptConsolePrinter(order.OrderItems, receiptCalculator.TotalSalesTax, receiptCalculator.TotalAfterTax);
                    printer.Output();
                    if (ValidatedYesNoPrompt("\nDo you want to start another order?\n"))
                    {
                        Console.WriteLine("NEW ORDER STARTED");
                        order = new Order();
                        receiptCalculator = new ReceiptCalculator();
                    } else
                    {
                        Console.WriteLine("\nAll orders complete. Thank you!\n");
                        ordersComplete = true;
                    }
                }
            }



        }

        static bool ValidatedYesNoPrompt(string msg)
        {
            var valid_ans_list = new List<string>() { "yes", "no" };
            string ans;
            do
            {
                Console.WriteLine(msg + " [yes/no]");
                ans = Console.ReadLine();
                if (!valid_ans_list.Contains(ans))
                {
                    Console.WriteLine("Error: Please answer with 'yes' or 'no'.");
                }
            } while (!valid_ans_list.Contains(ans));

            return (ans == "yes") ? true : false; 

        }

        static int ValidatedIntPrompt(string msg)
        {
            Console.WriteLine(msg);
            var valid_input = false;
            while (!valid_input) 
                try
                {
                    var ans = Convert.ToInt32(Console.ReadLine());
                    return ans;
                }
                catch
                {
                    Console.WriteLine("Error: Invalid input type.");
                    Console.WriteLine(msg);
                }

            return 0;
        }

        static decimal ValidatedDecimalPrompt(string msg, int decimal_places)
        {
            Console.WriteLine(msg);
            var valid_input = false;
            while (!valid_input)
                try
                {
                    var ans = Math.Round(Convert.ToDecimal(Console.ReadLine()), decimal_places);
                    return ans;
                }
                catch
                {
                    Console.WriteLine("Error: Invalid input type.");
                    Console.WriteLine(msg);
                }

            return 0;
        }

    }
}

