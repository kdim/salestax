﻿

using System.Collections.Generic;

namespace SalesTax.Src
{
    public abstract class ReceiptOutputHandler
    {
        public List<IOrderItem<TaxableItem>> OrderItems {get; set;}
        public decimal TotalSalesTax { get; set; }
        public decimal TotalAfterTax { get; set; }

        public ReceiptOutputHandler(List<IOrderItem<TaxableItem>> orderItems, decimal totalSalesTax, decimal totalAfterTax)
        {
            OrderItems = orderItems;
            TotalSalesTax = totalSalesTax;
            TotalAfterTax = totalAfterTax;
        }

        public abstract void Output();
    }
}
