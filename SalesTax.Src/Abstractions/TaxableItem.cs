﻿
using System;
using System.Collections.Generic;

namespace SalesTax.Src
{ 
    public abstract class TaxableItem
    {
        public string Name { get; set; }
        public virtual decimal Price { get; set; }
        public virtual Dictionary<string, decimal> ApplicableTaxes { get; set; }
        public virtual decimal SalesTax()
        {
            decimal taxPercentage = 0;
            foreach (var tax in ApplicableTaxes)
            {
                taxPercentage += tax.Value;
            }
            return Math.Ceiling(Math.Round((Price * taxPercentage / 100), 2) * 20) / 20;
        }

        public virtual decimal CostAfterTax() => Price + SalesTax();
    }
}
