using SalesTax.Src;
using System.Collections.Generic;
using Xunit;

namespace SalesTax.Tests
{
    public class Product_CanCalculateItsTax
    {

        [Theory]
        [MemberData(nameof(ProductDataForTexTests))]
        public void SalesTax_ReturnsCorrectValue(decimal price, Dictionary<string, decimal> taxes, decimal excpected)
        {
            #region Arrange
            var _product = new Product("", price)
            {
                Price = price,
                ApplicableTaxes = taxes
            };

            #endregion

            #region Act
            var result = _product.SalesTax();

            #endregion

            #region Assert
            Assert.Equal(excpected, result);

            #endregion

        }



        [Theory]
        [MemberData(nameof(ProductDataForCostAfterTax))]
        public void CostAfterTax_ReturnsCorrectValue(decimal price, Dictionary<string, decimal> taxes, decimal excpected)
        {
            #region Arrange
            var _product = new Product("", price)
            {
                Price = price,
                ApplicableTaxes = taxes
            };
 
            #endregion
 
            #region Act
            var result = _product.CostAfterTax();
 
            #endregion
 
            #region Assert
            Assert.Equal(excpected, result);
 
            #endregion
 
        }

        public static IEnumerable<object[]> ProductDataForTexTests()
        {
            yield return new object[]
            {
                27.99,
                new Dictionary<string, decimal>() { {"imported",5}, {"regular",10}},
                4.20
            };

            yield return new object[]
            {
                18.99,
                new Dictionary<string, decimal>() { {"regular",10} },
                1.90
            };

            yield return new object[]
            {
                9.75,
                new Dictionary<string, decimal>() { },
                0.00
            };

            yield return new object[]
            {
                11.25,
                new Dictionary<string, decimal>() { {"imported",5} },
                0.60
            };
        }

        public static IEnumerable<object[]> ProductDataForCostAfterTax()
        {
            yield return new object[]
{
                27.99,
                new Dictionary<string, decimal>() { {"imported",5}, {"regular",10}},
                32.19
            };

            yield return new object[]
            {
                18.99,
                new Dictionary<string, decimal>() { {"regular",10} },
                20.89
            };

            yield return new object[]
            {
                9.75,
                new Dictionary<string, decimal>() { },
                9.75
            };
            
            yield return new object[]
            {
                11.25,
                new Dictionary<string, decimal>() { {"imported",5} },
                11.85
            };
        }
    }
}
