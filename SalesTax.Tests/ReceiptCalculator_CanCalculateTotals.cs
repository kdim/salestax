﻿using System.Collections.Generic;
using SalesTax.Src;
using Xunit;
using NSubstitute;

namespace SalesTax.Tests
{
    public class ReceiptCalculator_CanCalculateTotals
    {

        private List<IOrderItem<TaxableItem>> _OrderItemsList = new List<IOrderItem<TaxableItem>>();

        private ReceiptCalculator _calculator = new ReceiptCalculator();

        private void arrange()
        {

            // 1 imported bottle of perfume at 27.99
            var stubOrderItem = Substitute.For<IOrderItem<TaxableItem>>();
            stubOrderItem.Product = Substitute.For<TaxableItem>();
            stubOrderItem.NumberOnCheckout.Returns(1);
            stubOrderItem.Product.SalesTax().Returns((decimal)4.20);
            stubOrderItem.Product.CostAfterTax().Returns((decimal)32.19);
            _OrderItemsList.Add(stubOrderItem);

            // 1 bottle of perfume at 18.99
            stubOrderItem = Substitute.For<IOrderItem<TaxableItem>>();
            stubOrderItem.Product = Substitute.For<TaxableItem>();
            stubOrderItem.NumberOnCheckout.Returns(1);
            stubOrderItem.Product.SalesTax().Returns((decimal)1.90);
            stubOrderItem.Product.CostAfterTax().Returns((decimal)20.89);
            _OrderItemsList.Add(stubOrderItem);

            // 1 packet of headache pills at 9.75 
            stubOrderItem = Substitute.For<IOrderItem<TaxableItem>>();
            stubOrderItem.Product = Substitute.For<TaxableItem>();
            stubOrderItem.NumberOnCheckout.Returns(1);
            stubOrderItem.Product.SalesTax().Returns((decimal)0.00);
            stubOrderItem.Product.CostAfterTax().Returns((decimal)9.75);
            _OrderItemsList.Add(stubOrderItem);

            // 1 box of imported chocolates at 11.25 
            stubOrderItem = Substitute.For<IOrderItem<TaxableItem>>();
            stubOrderItem.Product = Substitute.For<TaxableItem>();
            stubOrderItem.NumberOnCheckout.Returns(1);
            stubOrderItem.Product.SalesTax().Returns((decimal)0.60);
            stubOrderItem.Product.CostAfterTax().Returns((decimal)11.85);
            _OrderItemsList.Add(stubOrderItem);

        }


        [Fact]
        public void CalculateTotalSalesTax_RequiresRounding_RoundedTo5cents()
        {
            #region Arrange

            arrange();

            #endregion

            #region Act
            _calculator.ProcessOrder(_OrderItemsList);

            #endregion

            #region Assert
            Assert.Equal((decimal)6.70, _calculator.TotalSalesTax);

            #endregion

        }

        [Fact]
        public void CalculateTotalAfterTax_RoundedTo2Decimals()
        {
            #region Arrange
            arrange();

            #endregion

            #region Act
            _calculator.ProcessOrder(_OrderItemsList);

            #endregion

            #region Assert
            Assert.Equal((decimal)74.68, _calculator.TotalAfterTax);

            #endregion

        }

    }
}
